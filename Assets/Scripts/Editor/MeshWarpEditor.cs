using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MeshSineWarp))]
public class MeshWarpEditor : Editor
{
    private MeshSineWarp tgt;

    public void CreateAsset()
    {
        string filePath = EditorUtility.SaveFilePanelInProject("Save Mesh", "New_Area_Mesh", "Asset", "");
        if (filePath == "") return;
        AssetDatabase.CreateAsset(tgt.newMesh, filePath);
    }
    public override void OnInspectorGUI()
    {
        tgt = target as MeshSineWarp;
        if (tgt == null) return;
        GUI.color = Color.cyan;

        if (GUILayout.Button("Warp Mesh"))
        {
            tgt.WarpMesh();
            
        }
        if (GUILayout.Button("Store Mesh"))
        {
            tgt.StoreMesh();

        }
        if (GUILayout.Button("Restore Mesh"))
        {
            tgt.RestoreMesh();

        }
        if (GUILayout.Button("Create Asset Mesh"))
        {
            if(tgt.newMesh!=null)CreateAsset();

        }
        DrawDefaultInspector();
    }
}
